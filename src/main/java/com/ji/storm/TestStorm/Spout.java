package com.ji.storm.TestStorm;

import java.util.Map;

import com.ji.storm.TestStorm.cons.Const;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

@SuppressWarnings("serial")
public class Spout extends BaseRichSpout {
	private int index = 0;
	private SpoutOutputCollector collector;
	private String[] words = { "i love china", "in china and this is my work" };

	@Override
	@SuppressWarnings("rawtypes")
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void nextTuple() {
		if (index >= words.length) {
			index = 0;
			return;
		}
		collector.emit(new Values(words[index]));
		index++;
		Utils.waitForMillis(10000);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(Const.SENTENCE));
	}

}
