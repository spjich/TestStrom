package com.ji.storm.TestStorm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class TestStringWriter {

	public static void writeToTxt(String line) {
		File path = new File("/usr/local/program/strom/");
		File file = new File("/usr/local/program/strom/strom.txt");
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			if (!path.exists()) {
				path.mkdirs();
			}
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file, true);
			writer = new BufferedWriter(fw);
			writer.write(line);
			writer.newLine();// 换行
			writer.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		writeToTxt("hello");
		writeToTxt("hell2o");
	}

}